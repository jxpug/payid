/* eslint-disable node/no-process-env -- na */
/* eslint-disable @typescript-eslint/naming-convention -- na */
import config from './config'

interface ProcessEnv extends NodeJS.ProcessEnv {
  STAGE: string
  SLS_AWS_DYNAMODB_PAYID_TABLE: string
  SLS_AWS_API_KEY?: string
  SLS_AWS_API_SECRET?: string
  SLS_AWS_REGION: string
  SLS_REDIRECT_SITE: string
}

const processEnv = process.env as ProcessEnv

const extenedConfig = {
  ...config,
  dynamodbTableName: processEnv.SLS_AWS_DYNAMODB_PAYID_TABLE,
  awsApiKey: processEnv.SLS_AWS_API_KEY,
  awsSecret: processEnv.SLS_AWS_API_SECRET,
  awsRegion: processEnv.SLS_AWS_REGION,
  redirect: processEnv.SLS_REDIRECT_SITE,
}

export default extenedConfig

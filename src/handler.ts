/* eslint-disable import/prefer-default-export -- na */
/* eslint-disable import/no-unused-modules -- na */
import { APIGatewayProxyEvent, Context } from 'aws-lambda'
import * as awsServerlessExpress from 'aws-serverless-express'
import * as express from 'express'

import sendSuccess from './middlewares/sendSuccess'
import { adminApiRouter, publicApiRouter } from './routes'
import config from './serverless.config'

const privateServer: express.Application = express()
privateServer.use('/users', adminApiRouter)

const publicServer: express.Application = express()
publicServer.use('/status/health', sendSuccess)
publicServer.use('/', publicApiRouter)

const privateServerProxy = awsServerlessExpress.createServer(privateServer)
const publicServerProxy = awsServerlessExpress.createServer(publicServer)

/**
 * Handles the API Gateway request by proxying to express.
 *
 * @param event - A APIGatewayProxyEvent object.
 * @param context - A APIGateway Context object.
 * @returns A AWS Serverless Express response object.
 */
async function handlePrivateRequests(
  event: APIGatewayProxyEvent,
  context: Context,
): Promise<awsServerlessExpress.Response> {
  return awsServerlessExpress.proxy(
    privateServerProxy,
    event,
    context,
    'PROMISE',
  ).promise
}

/**
 * Handles the API Gateway request by proxying to express.
 *
 * @param event - A APIGatewayProxyEvent object.
 * @param context - A APIGateway Context object.
 * @returns A AWS Serverless Express response object.
 */
async function handlePublicRequests(
  event: APIGatewayProxyEvent,
  context: Context,
): Promise<awsServerlessExpress.Response> {
  return awsServerlessExpress.proxy(
    publicServerProxy,
    event,
    context,
    'PROMISE',
  ).promise
}

/**
 * Handles the API Gateway request by proxying to express.
 *
 * @param _event - A APIGatewayProxyEvent object.
 * @param _context - A APIGateway Context object.
 * @returns A AWS Serverless Express response object.
 */
async function handleRedirectToAppSite(
  _event: APIGatewayProxyEvent,
  _context: Context,
): Promise<awsServerlessExpress.Response> {
  return Promise.resolve({
    statusCode: 301,
    headers: {
      Location: config.redirect,
    },
    body: '',
  })
}

export {
  handlePrivateRequests as private,
  handlePublicRequests as public,
  handleRedirectToAppSite as redirect,
}
